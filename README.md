# SACodeKata

This should help outline my thought process while working through my solution

## Assumptions on what the program should do

Should be able to handle the requested commands regardless of upper or lower case.

Should be able to handle the names regardless of upper or lower case.

If a duplicate name is entered regardless of case when creating a driver, only accept the first input.

Assumes there are no special characters being used for the names.

Should still output the drivers distance if it's 0 miles if there is no speed present due to no valid trips.

Assumes each line of the txt file fits one of the two formats defined by the coding problem prompt


## Coding decisions

Using dictionary to store each driver’s info and using their name as a key to ensure unique drivers or stored based on the given name.

Dictionary key is stored with lowercase form of the driver name so that the same name can be checked when the Driver command is prompted to create a new driver.

Storing each driver’s information in a new class to make testing each driver’s information easier.

Implements comparable to use the sort method for collections and have a custom comparison between drivers based on their distance traveled.

Use NUNIT testing to test various single lines of input to see if it matches expected output

Created a defaultinput.txt file for testing if the general code works as expected when interacting with a .txt file

No end to end test included besides just running the main program with defaultInput.txt as the input file.

## Running the code

This is my first C# project so I'm not exactly sure what is a catch all way to run this.
