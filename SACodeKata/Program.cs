using System;
using System.Collections.Generic;
using System.IO;

namespace SACodeKata
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string filename = args[0];
            Dictionary<String, Driver> driverList = new Dictionary<string, Driver>();
            try
            {
                using (StreamReader fileReader = new StreamReader(filename))
                {
                    ReadFile(fileReader, driverList);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /*
         * Reads each line of the input.txt file and processes it to add information
         * to the list of drivers.
         */
        public static void ReadFile(StreamReader fileReader, Dictionary<string, Driver> driverList)
        {
            string line;
            while ((line = fileReader.ReadLine()) != null)
            {
                string[] inputArr = line.Split(" ");
                string command = inputArr[0].ToLower();
                switch (command)
                {
                    case "driver":
                        ProcessDriver(inputArr, driverList);
                        break;
                    case "trip" :
                        ProcessTrip(inputArr, driverList);
                        break;
                }
            }
            List<Driver> sortedList = SortDrivers(driverList);
            foreach (Driver driver in sortedList)
            {
                Console.WriteLine(driver);
            }
        }

        public static void ProcessDriver(string[] inputArr, Dictionary<string, Driver> driverList)
        {
            string driverName = inputArr[1].ToLower();
            if (!driverList.ContainsKey(driverName))
            {
                Driver newDriver = new Driver();
                String normalDriverName = driverName.Substring(0, 1).ToUpper() + driverName.Substring(1);
                newDriver.SetName(normalDriverName);
                driverList.Add(driverName, newDriver);
            }
        }

        public static void ProcessTrip(string[] inputArr, Dictionary<string, Driver> driverList)
        {
            string driverName = inputArr[1].ToLower();
            Driver existingDriver = driverList[driverName];
            double hoursTraveled = CalcHoursTraveled(inputArr[2].Split(":"),inputArr[3].Split(":"));
            double distance = Double.Parse(inputArr[4]);
            int speed = (int) Math.Round(distance / hoursTraveled, MidpointRounding.AwayFromZero);
            if (speed > 5 && speed < 100) {
                existingDriver.IncHoursTraveled(hoursTraveled);
                existingDriver.IncDistance(distance);
            }
        }

        public static double CalcHoursTraveled(string[] start, string[] end)
        {
            double startHour = Double.Parse(start[0]);
            double startMin = Double.Parse(start[1]);
            double endHour = Double.Parse(end[0]);
            double endMin = Double.Parse(end[1]);
            double passedMinutes = endMin - startMin;
            double passedHours = endHour - startHour + passedMinutes / 60;

            return passedHours;
        }

        public static List<Driver> SortDrivers(Dictionary<string, Driver> currentDrivers)
        {
            List<Driver> newList = new List<Driver>(currentDrivers.Values);
            newList.Sort();
            return newList;
        }
    }
}