﻿using System;
using System.Globalization;

namespace SACodeKata
{
    public class Driver : IComparable<Driver>
    {
        private string _name;
        private double _distance;
        private double _hoursTraveled;

        /*
     * setName
     *
     * @param name - new name for a driver
     */
    public void SetName(string name) {
        this._name = name;
    }

    /*
     * getName
     *
     * @return the name of the driver
     */
    public string GetName() {
        return this._name;
    }

    /*
     * setHours
     *
     * @param hours - hours traveled
     */
    public void IncHoursTraveled(double hours) {
        this._hoursTraveled += hours;
    }

    /*
     * getHoursTraveled
     *
     * @return the amount of the hours traveled
     */
    public Double GetHoursTraveled() {
        return this._hoursTraveled;
    }

    /*
     * setDistance
     *
     * @param distance - the distance traveled
     */
    public void IncDistance(double distance) {
        this._distance += distance;
    }

    /*
     * getDistance
     *
     * @return the total distance traveled
     */
    public double GetDistance() {
        return this._distance;
    }

    /*
     * getSpeed
     *
     * @return the average speed over the trip
     */
    public int GetSpeed() {
        int speed = (int) Math.Round(this._distance / this._hoursTraveled, MidpointRounding.AwayFromZero);
        return speed;
    }

    /**
     *
     * Drivers are ordered by their distance. A positive value is returned if
     * the distance is less than the one being compared to, negative if greater,
     * and 0 if it is equal.
     *
     */
    public int CompareTo(Driver other) {
        if (this._distance < other.GetDistance()) {
            return 1;
        } else if (this._distance > other.GetDistance()) {
            return -1;
        } else {
            return 0;
        }
    }

    /*
     * Returns tracking info for a driver to a string
     *
     * @return the tracking info for a driver
     */
    public override string ToString() {
        string info = this._name + ": ";
        info += Math.Round(this._distance) + " miles ";
        if (this._distance > 0) {
            info += "@ " + this.GetSpeed() + " mph";
        }
        return info;
    }

    /**
     * Two drivers are equal when their names are the same.
     *
     */
    public override bool Equals(object obj) {
        bool eq = true;
        if ((obj == null) || (this.GetType() != obj.GetType())) {
            eq = false;
        } else {
            Driver other = (Driver) obj;
            eq = this._name.Equals(other._name);
        }
        return eq;
    }
        
    }
}