using System;
using System.Collections.Generic;
using NUnit.Framework;
using SACodeKata;
using Program = SACodeKata.Program;


namespace TestSACodeKata
{
    public class ProgramTests
    {
        static string tripDan = "Trip Dan 07:15 07:45 17.3";
        static string tripDan2 = "Trip Dan 06:12 06:32 21.8";
        static string createDan = "Driver Dan";

        static string createLauren = "Driver Lauren";
        static string tripLauren = "Trip Lauren 12:01 13:16 42.0";
            
        static string createBob = "Driver Bob";
        static string breakBob = "Trip Bob 12:01 13:16 5.0";
        static string breakBob2 = "Trip Bob 12:01 13:16 -50.0";

        static string[] danArr = createDan.Split(" ");
        static  string[] danArrTrip = tripDan.Split(" ");
        string[] danArrTrip2 = tripDan2.Split(" ");

        string[] laurenArr = createLauren.Split(" ");
        string[] laurenArrTrip = tripLauren.Split(" ");

        string[] bobArr = createBob.Split(" ");
        string[] bobArrTrip = breakBob.Split(" ");
        string[] bobArrTrip2 = breakBob2.Split(" ");

        private Dictionary<string, Driver> _driverList;
        [SetUp]
        public void Setup()
        {
            _driverList = new Dictionary<string, Driver>();
        }

        [Test]
        public void CalculateHoursTraveledDan1()
        {
            double hours = Program.CalcHoursTraveled(danArrTrip[2].Split(":"), danArrTrip[3].Split(":"));
            Assert.IsTrue(hours==0.5);
        }

        [Test]
        public void CreateDriverCommand()
        {
            Program.ProcessDriver(danArr, _driverList);
            Program.ProcessTrip(danArrTrip, _driverList);
            Console.WriteLine(_driverList["dan"].ToString());
            
            Assert.IsTrue(_driverList["dan"].ToString().Equals("Dan: 17 miles @ 35 mph"));
        }

        [Test]
        public void AddTripsOneDriver()
        {
            Program.ProcessDriver(danArr, _driverList);
            Program.ProcessTrip(danArrTrip, _driverList);
            Program.ProcessTrip(danArrTrip2, _driverList);
            
            Assert.IsTrue(_driverList["dan"].ToString().Equals("Dan: 39 miles @ 47 mph"));
        }
        
        [Test]
        public void AddTripsTwoDrivers()
        {
            Program.ProcessDriver(danArr, _driverList);
            Program.ProcessTrip(danArrTrip, _driverList);
            Program.ProcessTrip(danArrTrip2, _driverList);
            
            Program.ProcessDriver(laurenArr, _driverList);
            Program.ProcessTrip(laurenArrTrip, _driverList);

            Assert.IsTrue(_driverList["dan"].ToString().Equals("Dan: 39 miles @ 47 mph"));
            Assert.IsTrue(_driverList["lauren"].ToString().Equals("Lauren: 42 miles @ 34 mph"));
        }
        
        [Test]
        public void AddTripsThreeDrivers()
        {
            Program.ProcessDriver(danArr, _driverList);
            Program.ProcessTrip(danArrTrip, _driverList);
            Program.ProcessTrip(danArrTrip2, _driverList);
            
            Program.ProcessDriver(laurenArr, _driverList);
            Program.ProcessTrip(laurenArrTrip, _driverList);
            
            Program.ProcessDriver(bobArr, _driverList);
            Program.ProcessTrip(bobArrTrip, _driverList);
            Program.ProcessTrip(bobArrTrip2, _driverList);

            Assert.IsTrue(_driverList["dan"].ToString().Equals("Dan: 39 miles @ 47 mph"));
            Assert.IsTrue(_driverList["lauren"].ToString().Equals("Lauren: 42 miles @ 34 mph"));
            Assert.IsTrue(_driverList["bob"].ToString().Equals("Bob: 0 miles "));
        }

        [Test]
        public void SortThreeDrivers()
        {
            Program.ProcessDriver(bobArr, _driverList);
            Program.ProcessTrip(bobArrTrip, _driverList);
            Program.ProcessTrip(bobArrTrip2, _driverList);

            Program.ProcessDriver(laurenArr, _driverList);
            Program.ProcessTrip(laurenArrTrip, _driverList);
            
            Program.ProcessDriver(danArr, _driverList);
            Program.ProcessTrip(danArrTrip, _driverList);
            Program.ProcessTrip(danArrTrip2, _driverList);

            List<Driver> sortedList = Program.SortDrivers(_driverList);
            Console.WriteLine(string.Join(", ",sortedList));

            string sortedListStr = string.Join(", ",sortedList);
            
            Assert.IsTrue(sortedListStr.Equals("Lauren: 42 miles @ 34 mph, Dan: 39 miles @ 47 mph, Bob: 0 miles "));
        }

    }
}