using System;
using System.Collections.Generic;
using NUnit.Framework;
using Driver = SACodeKata.Driver;

namespace TestSACodeKata
{
    public class DriverUnitTests
    {
        private Driver _driver;
        private string _name = "bobby";
        private double _dist = 50;
        private double _hours = 20;
        
        [SetUp]
        public void Setup()
        {
            _driver = new Driver();
        }
        
        [Test]
        public void SetAndGetName()
        {
            _driver.SetName(_name);
            Assert.IsTrue(_driver.GetName().Equals(_name));
        }

        [Test]
        public void SetAndGetHours()
        {
            _driver.IncHoursTraveled(_hours);
            Assert.IsTrue(_driver.GetHoursTraveled()==_hours);
        }

        [Test]
        public void SetAndGetDistance()
        {
            _driver.IncDistance(_dist);
            Assert.IsTrue(_driver.GetDistance()==_dist);
        }
        
        [Test]
        public void GetSpeed()
        {
            _driver.IncDistance(_dist);
            _driver.IncHoursTraveled(_hours);
            Console.WriteLine(_driver.GetSpeed());
            Assert.IsTrue(_driver.GetSpeed() == 3);
        }
        
        [Test]
        public void TestToString()
        {
            _driver.SetName(_name);
            _driver.IncDistance(_dist);
            _driver.IncHoursTraveled(_hours);
            Console.WriteLine(_driver.ToString());
            Assert.IsTrue(_driver.ToString().Equals("bobby: 50 miles @ 3 mph"));
        }
        
        [Test]
        public void TestEquals()
        {
            _driver.SetName(_name);
            _driver.IncDistance(_dist);
            _driver.IncHoursTraveled(_hours);
            Driver driver2 = new Driver();
            
            driver2.SetName("bobby");
            Assert.IsTrue(_driver.Equals(driver2));
            
            driver2.SetName("bob");
            Assert.IsFalse(_driver.Equals(driver2));
        }

    }
}